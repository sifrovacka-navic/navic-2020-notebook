SH=/bin/bash
TEX=lualatex -interaction=nonstopmode

all: notebook

tools: 
	cd tools && $(MAKE)

cover: notebook-cover.pdf

sheet:
	inkscape notebook-sheet.svg --export-pdf=notebook-sheet.pdf
	
%.pdf: %.tex 
	rm -f %.xmpdata %.aux %.pdf
	$(TEX) $<
	$(TEX) $<
	
convert:
	convert -density 1200 notebook-cover.pdf -resize 25% -unsharp 0x1 +append -background white -alpha remove -alpha off notebook-cover.png
	gs -o notebook-cover-fonts.pdf -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -c ".setpdfwrite <</NeverEmbed [ ]>> setdistillerparams" -f notebook-cover.pdf
	gs -o notebook-cover-cmyk.pdf -sDEVICE=pdfwrite -r2400 -dOverrideICC=true -sOutputICCProfile=/usr/share/color/icc/ghostscript/ps_cmyk.icc -sColorConversionStrategy=CMYK -dProcessColorModel=/DeviceCMYK -dRenderIntent=3 -dDeviceGrayToK=true -f notebook-cover-fonts.pdf
	rm notebook-cover-fonts.pdf
	gs -o notebook-sheet-grayscale.pdf -sDEVICE=pdfwrite -sProcessColorModel=DeviceGray -sColorConversionStrategy=Gray -dOverrideICC -f notebook-sheet.pdf
	
clean:
	rm -f *.log *.dvi *.aux *.toc *.lof *.lot *.out *.bbl *.blg *.xmpi *.nlo *.nls *.pdf *.xmpdata

notebook: tools cover sheet convert

blok: notebook

light: tools-light cover sheet convert

tools-light: 
	cd tools && $(MAKE) light
